//
//  EventDateExtension.swift
//  Torpedo
//
//  Created by Artem Soloviev on 07.11.2023.
//

import Foundation

extension Date {
    var eventDate: String {
        self.formatted(
            .dateTime.day().month(.wide).hour().minute().locale(Locale(identifier: "ru_RU"))
        )
        
    }
}
