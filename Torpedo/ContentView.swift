//
//  ContentView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 09.10.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
    @EnvironmentObject var team: TeamViewModel
    @StateObject var event = EventViewModel()
    
    init() {
        let tab = UITabBarAppearance()
        tab.backgroundColor = UIColor(Color("MainColor"))
//        UITabBar.appearance().scrollEdgeAppearance = tab
        UITabBar.appearance().standardAppearance = tab
            let nav = UINavigationBarAppearance()
        nav.backgroundColor = UIColor(Color("MainColor"))
//        UINavigationBar.appearance().scrollEdgeAppearance = nav
        UINavigationBar.appearance().standardAppearance = nav
        }

    var mainTab: some View {
        MainView(evens: event)
    }
  
    var teamTab: some View {
        MainTeamView()
    }
//    var shopTab: some View {
//        MainShopView()
//    }
//    var ticketsTab: some View {
//        MainTicketView()
//    }
    var eventsTab: some View {
        EventDetailView(event: event)
    }

    var body: some View {

        TabView(selection: $selection){

            mainTab.tabItem {
                
//                Label("Главная", systemImage: "sportscourt.fill")
                Label("Главная", systemImage: "hockey.puck.fill")
//                Label("Главная", systemImage: "hockey.puck")
            }
            .tag(0)
            
            teamTab.tabItem {
//                Label("Команда", systemImage: "person.3.fill")
                Label("Команда", systemImage: "figure.hockey")
            }
            .tag(1)
            eventsTab.tabItem{
                Label("Игры", systemImage: "sportscourt.fill")
            }
            .tag(2)
           
//            shopTab.tabItem{
//                Label("Магазин", systemImage: "bag.fill")
//            }
//            .tag(2)
//            ticketsTab.tabItem{
//                Label("Билеты", systemImage: "wallet.pass.fill")
//            }
//            .tag(3)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
