//
//  EventsViewModel.swift
//  Torpedo
//
//  Created by Artem Soloviev on 27.10.2023.
//    GET /api/khl_mobile/events_v2.json?application=khl_ios&deviceid=3E52D2EA-B0E2-48F1-8211-8245E5171A5B&locale=en&order_direction=asc&q%5Bstart_at_gt_time_from_unixtime%5D=1696867200&q%5Bteam_a_or_team_b_eq%5D=22&stage_id=275 HTTP/1.1

import Foundation

@MainActor
final class EventViewModel: ObservableObject {
    
    @Published var results:[Event] = []
    @Published var nearGames: [Event] = []
    
    init()  {
        Task {
            await loadData()
        }
//        print(nearGames.count)
    }

    private func loadData () async {
        let date = Date()
        let dateUNITX = Int(date.timeIntervalSince1970 - 2629743)
            print(dateUNITX)
        guard let url = URL(string:
"https://khl.api.webcaster.pro/api/khl_mobile/events_v2.json?locale=ru&order_direction=asc&q%5Bstart_at_gt_time_from_unixtime%5D=\(dateUNITX)&q%5Bteam_a_or_team_b_eq%5D=22&stage_id=275") else {
            print ("invalid URL")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .millisecondsSince1970
            let decodedResult = try decoder.decode([WelcomeElement].self, from: data)
            
            for element in decodedResult {
                results.append(element.event)
                nearGames = results.filter({$0.gameStateKey == .notYetStarted}).sorted {$0.eventStartAt < $1.eventStartAt}
                
            }
            
        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
            
        }
    }
}
