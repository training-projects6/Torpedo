//
//  StageViewModel.swift
//  Torpedo
//
//  Created by Artem Soloviev on 16.11.2023.
//

import Foundation


@MainActor
final class StageViewModel: ObservableObject {
    
    
    @Published var stage: [Championship] = []
    
    init()  {
        Task {
            await loadData()
        }
    }
    
    private func loadData () async {
        guard let url = URL(string: "https://khl.api.webcaster.pro/api/khl_mobile/tables_v2.json?locale=ru&stage_id=275") else {
            print ("invalid URL")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .millisecondsSince1970
            let decodedResult = try decoder.decode([Element].self, from: data)
            let sortedDecodedResult = decodedResult.filter({$0.season == "2023/2024"})
            for element in sortedDecodedResult {
                for item in element.tables.championship{
                    stage.append(item)
                    stage = stage.sorted {$0.pts > $1.pts}
                }
            }
            
            
        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
            
        }
    }
}
