//
//  TeamViewModel.swift
//  Torpedo
//
//  Created by Artem Soloviev on 27.10.2023.
//    GET /api/khl_mobile/team_v2.json?application=khl_ios&cacheid=event_1929311%3A1698593063.115356&deviceid=3E52D2EA-B0E2-48F1-8211-8245E5171A5B&id=22&locale=en&stage_id=275 HTTP/1.1

import Foundation


@MainActor
final class TeamViewModel: ObservableObject {
    
    @Published var players:[Player] = []
    @Published var bestPlayers:[Player] = []
    @Published var playerStat:[PlayerStat] = []
    @Published var stat:[Stat] = []

    @Published var isPresented = false
    
    init() {
        Task {
            await getID()
        }
    }
    

    func getID () async {
        guard let url = URL(string: "https://khl.api.webcaster.pro/api/khl_mobile/team_v2.json?id=22&locale=ru") else {
            print ("invalid URL")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            
            let decodedResult = try JSONDecoder().decode(Welcome.self, from: data)

            bestPlayers = decodedResult.team.players.sorted {($0.g + $0.a) > ($1.g + $1.a)}
            players = decodedResult.team.players.sorted{ $0.roleKey.rawValue > $1.roleKey.rawValue }

        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
        }
    }
    
    func loadPlayerStats (id:Int) async {
        
        guard let url = URL(string: "https://khl.api.webcaster.pro/api/khl_mobile/players_v2.json?application=khl_ios&deviceid=3E52D2EA-B0E2-48F1-8211-8245E5171A5B&locale=ru&q%5Bid_in%5D%5B%5D=\(id)") else {
            print ("invalid URL")
            return
        }
        
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            
            let decodedResult = try JSONDecoder().decode([PlayerStats].self, from: data)
            
            for stats in decodedResult  {
                
                if !playerStat.isEmpty {
                    playerStat[0] = stats.player
                }else{
                    playerStat.append(stats.player)
                }
            }
            for player in playerStat {
                stat = player.stats!.filter({$0.id == "gp" || $0.id == "g" ||  $0.id == "pts" || $0.id == "a" ||  $0.id == "toi_avg" ||  $0.id == "fow" ||  $0.id == "pm" ||  $0.id == "top_speed" ||  $0.id == "time_on_ice" ||  $0.id == "distance_travelled" })
            }
            isPresented.toggle()
            
        }catch{
            print ("invalid data")
            print("Unexpected error: \(error).")
        }
    }
    func stats(id: Int) {
        Task{
            await loadPlayerStats(id:id)
        }
    }
}
