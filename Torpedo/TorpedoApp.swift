//
//  TorpedoApp.swift
//  Torpedo
//
//  Created by Artem Soloviev on 09.10.2023.
//

import SwiftUI

@main
struct TorpedoApp: App {
    
    @StateObject private var team = TeamViewModel()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(team)
        }
    }
}
