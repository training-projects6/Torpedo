//
//  Team.swift
//  Torpedo
//
//  Created by Artem Soloviev on 27.10.2023.
//

import Foundation


// MARK: - Welcome
//struct Welcome: Codable {
//    let team: Team
//}
//
//// MARK: - Team
//struct Team: Codable {
//    let id: Int
//    let stage: String
//    let khlID: Int
//    let name, location: String
//    let feedItems: [FeedItem]
//    let arena: Arena
//    let arenas: [Arena]
//    let calendarEvents: [CalendarEvent]
//    let recentEvents: [RecentEvent]
//    let website: String
//    let mail, foundationYear, photo, phone: String
//    let headCoach: HeadCoach
//    let socialNetworks: SocialNetworks
//    let apps: Apps
//    let address: String
//    let image: String
//    let division, divisionKey, conference, conferenceKey: String
//    let stats: [Stat]
//    let players: [Player]
//
//    enum CodingKeys: String, CodingKey {
//        case id, stage
//        case khlID = "khl_id"
//        case name, location
//        case feedItems = "feed_items"
//        case arena, arenas
//        case calendarEvents = "calendar_events"
//        case recentEvents = "recent_events"
//        case website, mail
//        case foundationYear = "foundation_year"
//        case photo, phone
//        case headCoach = "head_coach"
//        case socialNetworks = "social_networks"
//        case apps, address, image, division
//        case divisionKey = "division_key"
//        case conference
//        case conferenceKey = "conference_key"
//        case stats, players
//    }
//}
//
//// MARK: - Apps
//struct Apps: Codable {
//    let ios, android, windows: String
//}
//
//// MARK: - Arena
//struct Arena: Codable {
//    let id, capacity: Int
//    let phone: String
//    let website: String
//    let name, address, city: String
//    let image: String
//    let geo: Geo
//}
//
//// MARK: - Geo
//struct Geo: Codable {
//    let lat, long: String
//}
//
//// MARK: - CalendarEvent
//struct CalendarEvent: Codable {
//    let event: CalendarEventEvent
//}
//
//// MARK: - CalendarEventEvent
//struct CalendarEventEvent: Codable {
//    let gameStateKey: String
//    let period: Int?
//    let hd: Bool
//    let id, stageID, typeID: Int
//    let commentator: Bool
//    let likesEnabled: JSONNull?
//    let khlID: Int
//    let teamA, teamB: TeamAClass
//    let yandexRights: Bool
//    let name: String
//    let tickets: JSONNull?
//    let location: String
//    let startAtDay, startAt, eventStartAt, endAt: Int
//    let notRegular: Bool
//    let score: String
//    let scores: Scores
//    let sscore: JSONNull?
//    let image: String
//    let condensedGameID, highlightID: JSONNull?
//    let infographics: String
//    let infographicsEnabled, hasVideo: Bool
//    let m3U8URL, feedURL, iframeURL, iframeCode: JSONNull?
//
//    enum CodingKeys: String, CodingKey {
//        case gameStateKey = "game_state_key"
//        case period, hd, id
//        case stageID = "stage_id"
//        case typeID = "type_id"
//        case commentator
//        case likesEnabled = "likes_enabled"
//        case khlID = "khl_id"
//        case teamA = "team_a"
//        case teamB = "team_b"
//        case yandexRights = "yandex_rights"
//        case name, tickets, location
//        case startAtDay = "start_at_day"
//        case startAt = "start_at"
//        case eventStartAt = "event_start_at"
//        case endAt = "end_at"
//        case notRegular = "not_regular"
//        case score, scores, sscore, image
//        case condensedGameID = "condensed_game_id"
//        case highlightID = "highlight_id"
//        case infographics
//        case infographicsEnabled = "infographics_enabled"
//        case hasVideo = "has_video"
//        case m3U8URL = "m3u8_url"
//        case feedURL = "feed_url"
//        case iframeURL = "iframe_url"
//        case iframeCode = "iframe_code"
//    }
//}
//
//// MARK: - Scores
//struct Scores: Codable {
//    let firstPeriod, secondPeriod, thirdPeriod, overtime: String?
//    let bullitt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case firstPeriod = "first_period"
//        case secondPeriod = "second_period"
//        case thirdPeriod = "third_period"
//        case overtime, bullitt
//    }
//}
//
//// MARK: - TeamAClass
//struct TeamAClass: Codable {
//    let id: Int
//    let khlID: Int?
//    let image: String
//    let name, location: String
//    let tvImage: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case khlID = "khl_id"
//        case image, name, location
//        case tvImage = "tv_image"
//    }
//}
//
//// MARK: - FeedItem
//struct FeedItem: Codable {
//    let type: String
//    let date, id: Int
//    let title: String
//    let image: String
//    let body, bodyWMedia: String
//    let outerURL: String
//
//    enum CodingKeys: String, CodingKey {
//        case type, date, id, title, image, body
//        case bodyWMedia = "body_w_media"
//        case outerURL = "outer_url"
//    }
//}
//
//// MARK: - HeadCoach
//struct HeadCoach: Codable {
//    let photo: String
//    let name: String
//}
//
//// MARK: - Player
//struct Player: Codable {
//    let id, khlID: Int
//    let image: String
//    let flagImageURL: String
//    let g, a: Int
//    let roleKey: RoleKey
//    let name: String
//    let shirtNumber: Int
//    let country: Country
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case khlID = "khl_id"
//        case image
//        case flagImageURL = "flag_image_url"
//        case g, a
//        case roleKey = "role_key"
//        case name
//        case shirtNumber = "shirt_number"
//        case country
//    }
//}
//
//enum Country: String, Codable {
//    case belarus = "Belarus"
//    case japan = "Japan"
//    case russia = "Russia"
//    case slovakia = "Slovakia"
//    case usa = "USA"
//}
//
//enum RoleKey: String, Codable {
//    case defensemen = "defensemen"
//    case forward = "forward"
//    case goaltender = "goaltender"
//}
//
//// MARK: - RecentEvent
//struct RecentEvent: Codable {
//    let event: RecentEventEvent
//}
//
//// MARK: - RecentEventEvent
//struct RecentEventEvent: Codable {
//    let id: Int
//    let score: String
//    let typeID: Int
//    let gameStateKey: String
//    let period, khlID: Int
//    let teamA, teamB: TeamAClass
//    let name: String
//    let startAt, endAt, startAtDay: Int
//    let scores: Scores
//
//    enum CodingKeys: String, CodingKey {
//        case id, score
//        case typeID = "type_id"
//        case gameStateKey = "game_state_key"
//        case period
//        case khlID = "khl_id"
//        case teamA = "team_a"
//        case teamB = "team_b"
//        case name
//        case startAt = "start_at"
//        case endAt = "end_at"
//        case startAtDay = "start_at_day"
//        case scores
//    }
//}
//
//// MARK: - SocialNetworks
//struct SocialNetworks: Codable {
//    let tw, vk, ok, fb: String
//    let instagram, youtube, telegram: String
//}
//
//// MARK: - Stat
//struct Stat: Codable {
//    let id, title: String
//    let val, max: Int
//}
//
//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
