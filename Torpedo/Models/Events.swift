////
////  Events.swift
////  Torpedo
////
////  Created by Artem Soloviev on 27.10.2023.
////
//
import Foundation
//
//
//
//
//
//
//// This file was generated from JSON Schema using quicktype, do not modify it directly.
//// To parse the JSON, add this file to your project and do:
////
////   let welcome = try? JSONDecoder().decode(Welcome.self, from: jsonData)
//
//import Foundation
//
// MARK: - WelcomeElement
//struct Events: Codable {
//    let events: [Event]
//}
////struct Well2: Codable{
////    let evens2: Well
////}
//struct Well: Codable {
//    let events: [Event]
//
//}
struct WelcomeElement: Codable, Identifiable {
    let id = UUID()
    var event: Event
    enum CodingKeys: String, CodingKey {
        case event
    }
}

// MARK: - Event
struct Event: Codable, Identifiable, Hashable {
    static func == (lhs: Event, rhs: Event) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    

    
    let gameStateKey: GameStateKey
//    let period: Int?
//    let hd: Bool
    let id: Int
//    let stageID: Int
//let typeID: Int
//    let commentator: Bool
//    let likesEnabled: nil?
    let khlID: Int
    let teamA: Team2
        let teamB: Team2
//    let yandexRights: Bool
    let name: String
//    let tickets: String?
    let location: String
//    let startAtDay: Date
//    let startAt: Date
    let eventStartAt: Date
//let endAt: Int
//    let notRegular: Bool
    let score: String
//    let scores: Scores?
//    let sscore: JSONNull?
    let image: String
//    let condensedGameID, highlightID: Int?
//    let infographics: Infographics?
//    let infographicsEnabled, hasVideo: Bool
//    let m3U8URL, feedURL, iframeURL, iframeCode: JSONNull?

    enum CodingKeys: String, CodingKey {
        case gameStateKey = "game_state_key"
//        case period
//             case hd
        case id
//        case stageID = "stage_id"
//        case typeID = "type_id"
//        case commentator
//        case likesEnabled = "likes_enabled"
        case khlID = "khl_id"
        case teamA = "team_a"
        case teamB = "team_b"
//        case yandexRights = "yandex_rights"
        case name
//             case tickets
        case location
//        case startAtDay = "start_at_day"
//        case startAt = "start_at"
        case eventStartAt = "event_start_at"
//        case endAt = "end_at"
//        case notRegular = "not_regular"
        case score
//        scores
//             case sscore
                  case image
//        case condensedGameID = "condensed_game_id"
//        case highlightID = "highlight_id"
//        case infographics
//        case infographicsEnabled = "infographics_enabled"
//        case hasVideo = "has_video"
//        case m3U8URL = "m3u8_url"
//        case feedURL = "feed_url"
//        case iframeURL = "iframe_url"
//        case iframeCode = "iframe_code"
    }
}

enum GameStateKey: String, Codable {
    case finished = "finished"
    case notYetStarted = "not_yet_started"
    case inProgress = "in_progress"
}

enum Infographics: String, Codable {
    case withScoreboard = "With scoreboard"
    case withScoreboardWithoutTimer = "With scoreboard, without timer"
}

// MARK: - Scores
struct Scores: Codable {
    let firstPeriod, secondPeriod, thirdPeriod, overtime: String?
    let bullitt: String?

    enum CodingKeys: String, CodingKey {
        case firstPeriod = "first_period"
        case secondPeriod = "second_period"
        case thirdPeriod = "third_period"
        case overtime, bullitt
    }
}

// MARK: - Team
struct Team2: Codable {
    let id, khlID: Int
    let image: String
    let name, location: String

    enum CodingKeys: String, CodingKey {
        case id
        case khlID = "khl_id"
        case image, name, location
    }
}

