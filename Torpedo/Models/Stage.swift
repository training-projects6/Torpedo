//
//  Stage.swift
//  Torpedo
//
//  Created by Artem Soloviev on 16.11.2023.
//

import Foundation


struct Element: Codable {
    let season: String
//    let stages: [Stage]
    let tables: Tables
}

// MARK: - Stage
//struct Stage: Codable {
//    let id: Int
//    let title: StageTitle
//    let type: TypeEnum
//    let regular: [Championship]?
//    let playin: JSONNull?
//    let playoff: [Playoff]?
//}

// MARK: - Playoff
struct Playoff: Codable {
    let title: PlayoffTitle
    let level: Int
    let pairs: [Pair]
}

// MARK: - Pair
struct Pair: Codable {
    let games: [Game]
    let winner: Int?
    let score: [Int]
    let teams: [TeamA]
    let teamA, teamB: Team

    enum CodingKeys: String, CodingKey {
        case games, winner, score, teams
        case teamA = "team_a"
        case teamB = "team_b"
    }
}

// MARK: - Game
struct Game: Codable {
    let score: String
    let visitor, eventID: Int
    let ots: Ots?

    enum CodingKeys: String, CodingKey {
        case score, visitor
        case eventID = "event_id"
        case ots
    }
}

enum Ots: String, Codable {
    case ot = "OT"
}

// MARK: - Team
struct TeamA: Codable {
    let tvImage: TvImage
    let id, khlID: Int
    let name: Name
    let pos: Int
    let image: String
    let conferenceKey: ConferenceKey

    enum CodingKeys: String, CodingKey {
        case tvImage = "tv_image"
        case id
        case khlID = "khl_id"
        case name, pos, image
        case conferenceKey = "conference_key"
    }
}

enum ConferenceKey: String, Codable {
    case east = "east"
    case west = "west"
}

enum Name: String, Codable {
    case авангард = "Авангард"
    case автомобилист = "Автомобилист"
    case адмирал = "Адмирал"
    case акБарс = "Ак Барс"
    case амур = "Амур"
    case атлант = "Атлант"
    case барыс = "Барыс"
    case витязь = "Витязь"
    case динамоМ = "Динамо М"
    case динамоМн = "Динамо Мн"
    case динамоМск = "Динамо Мск"
    case динамоР = "Динамо Р"
    case донбасс = "Донбасс"
    case йокерит = "Йокерит"
    case куньлуньРС = "Куньлунь РС"
    case лада = "Лада"
    case левПопрад = "Лев Попрад"
    case левПр = "Лев Пр"
    case локомотив = "Локомотив"
    case медвешчак = "Медвешчак"
    case металлургМг = "Металлург Мг"
    case металлургНк = "Металлург Нк"
    case нефтехимик = "Нефтехимик"
    case салаватЮлаев = "Салават Юлаев"
    case северсталь = "Северсталь"
    case сибирь = "Сибирь"
    case ска = "СКА"
    case слован = "Слован"
    case спартак = "Спартак"
    case торпедо = "Торпедо"
    case трактор = "Трактор"
    case химикВс = "Химик Вс"
    case хкДинамоМ = "ХК Динамо М"
    case хкМвд = "ХК МВД"
    case хкСочи = "ХК Сочи"
    case цска = "ЦСКА"
    case югра = "Югра"
}

enum TvImage: String, Codable {
    case imagesKhlTvTeams105PNG = "/images/khl_tv/teams/105.png"
    case imagesKhlTvTeams109PNG = "/images/khl_tv/teams/109.png"
    case imagesKhlTvTeams10PNG = "/images/khl_tv/teams/10.png"
    case imagesKhlTvTeams113PNG = "/images/khl_tv/teams/113.png"
    case imagesKhlTvTeams12PNG = "/images/khl_tv/teams/12.png"
    case imagesKhlTvTeams14PNG = "/images/khl_tv/teams/14.png"
    case imagesKhlTvTeams16PNG = "/images/khl_tv/teams/16.png"
    case imagesKhlTvTeams18PNG = "/images/khl_tv/teams/18.png"
    case imagesKhlTvTeams20PNG = "/images/khl_tv/teams/20.png"
    case imagesKhlTvTeams22PNG = "/images/khl_tv/teams/22.png"
    case imagesKhlTvTeams24PNG = "/images/khl_tv/teams/24.png"
    case imagesKhlTvTeams26PNG = "/images/khl_tv/teams/26.png"
    case imagesKhlTvTeams287PNG = "/images/khl_tv/teams/287.png"
    case imagesKhlTvTeams28PNG = "/images/khl_tv/teams/28.png"
    case imagesKhlTvTeams291PNG = "/images/khl_tv/teams/291.png"
    case imagesKhlTvTeams30PNG = "/images/khl_tv/teams/30.png"
    case imagesKhlTvTeams315PNG = "/images/khl_tv/teams/315.png"
    case imagesKhlTvTeams32PNG = "/images/khl_tv/teams/32.png"
    case imagesKhlTvTeams34PNG = "/images/khl_tv/teams/34.png"
    case imagesKhlTvTeams36PNG = "/images/khl_tv/teams/36.png"
    case imagesKhlTvTeams38PNG = "/images/khl_tv/teams/38.png"
    case imagesKhlTvTeams40PNG = "/images/khl_tv/teams/40.png"
    case imagesKhlTvTeams42PNG = "/images/khl_tv/teams/42.png"
    case imagesKhlTvTeams44PNG = "/images/khl_tv/teams/44.png"
    case imagesKhlTvTeams46PNG = "/images/khl_tv/teams/46.png"
    case imagesKhlTvTeams471PNG = "/images/khl_tv/teams/471.png"
    case imagesKhlTvTeams48PNG = "/images/khl_tv/teams/48.png"
    case imagesKhlTvTeams50PNG = "/images/khl_tv/teams/50.png"
    case imagesKhlTvTeams521PNG = "/images/khl_tv/teams/521.png"
    case imagesKhlTvTeams52PNG = "/images/khl_tv/teams/52.png"
    case imagesKhlTvTeams54PNG = "/images/khl_tv/teams/54.png"
    case imagesKhlTvTeams56PNG = "/images/khl_tv/teams/56.png"
    case imagesKhlTvTeams58PNG = "/images/khl_tv/teams/58.png"
    case imagesKhlTvTeams61PNG = "/images/khl_tv/teams/61.png"
    case imagesKhlTvTeams645PNG = "/images/khl_tv/teams/645.png"
    case imagesKhlTvTeams65PNG = "/images/khl_tv/teams/65.png"
    case imagesKhlTvTeams73PNG = "/images/khl_tv/teams/73.png"
    case imagesKhlTvTeams8PNG = "/images/khl_tv/teams/8.png"
}

enum PlayoffTitle: String, Codable {
    case the12ФиналаКонференцияВосток = "1/2 финала, конференция «Восток»"
    case the12ФиналаКонференцияЗапад = "1/2 финала, конференция «Запад»"
    case the14ФиналаКонференцияВосток = "1/4 финала, конференция «Восток»"
    case the14ФиналаКонференцияЗапад = "1/4 финала, конференция «Запад»"
    case финал = "Финал"
    case финалКонференцияВосток = "Финал, конференция «Восток»"
    case финалКонференцияЗапад = "Финал, конференция «Запад»"
}

// MARK: - Championship
struct Championship: Codable, Identifiable, Hashable {
    let id: Int
    let tvImage: TvImage
    let khlID: Int
    let gp, w, otw, sow: String
    let sol, otl, l, pts: String
    let ptsPct: String?
    let gf, ga: String
    let image: String
    let name: Name
    let location: Location
    let division: DivisionEnum
    let divisionKey: Key
    let conference: Conference
    let conferenceKey: ConferenceKey
    let soa, so, pim, pima: String?

    enum CodingKeys: String, CodingKey {
        case id
        case tvImage = "tv_image"
        case khlID = "khl_id"
        case gp, w, otw, sow, sol, otl, l, pts
        case ptsPct = "pts_pct"
        case gf, ga, image, name, location, division
        case divisionKey = "division_key"
        case conference
        case conferenceKey = "conference_key"
        case soa, so, pim, pima
    }
}

enum Conference: String, Codable {
    case конференцияВосток = "Конференция «Восток»"
    case конференцияЗапад = "Конференция «Запад»"
}

enum DivisionEnum: String, Codable {
    case дивизионБоброва = "Дивизион Боброва"
    case дивизионТарасова = "Дивизион Тарасова"
    case дивизионХарламова = "Дивизион Харламова"
    case дивизионЧернышева = "Дивизион Чернышева"
}

enum Key: String, Codable {
    case bobrov = "bobrov"
    case chernyshev = "chernyshev"
    case kharlamov = "kharlamov"
    case tarasov = "tarasov"
}

enum Location: String, Codable {
    case астана = "Астана"
    case братислава = "Братислава"
    case владивосток = "Владивосток"
    case воскресенск = "Воскресенск"
    case донецк = "Донецк"
    case екатеринбург = "Екатеринбург"
    case загреб = "Загреб"
    case казань = "Казань"
    case магнитогорск = "Магнитогорск"
    case минск = "Минск"
    case москва = "Москва"
    case московскаяОбласть = "Московская область"
    case нижнекамск = "Нижнекамск"
    case нижнийНовгород = "Нижний Новгород"
    case новокузнецк = "Новокузнецк"
    case новосибирскаяОбласть = "Новосибирская область"
    case омск = "Омск"
    case пекин = "Пекин"
    case попрад = "Попрад"
    case прага = "Прага"
    case рига = "Рига"
    case санктПетербург = "Санкт-Петербург"
    case сочи = "Сочи"
    case тольятти = "Тольятти"
    case уфа = "Уфа"
    case хабаровск = "Хабаровск"
    case хантыМансийск = "Ханты-Мансийск"
    case хельсинки = "Хельсинки"
    case челябинск = "Челябинск"
    case череповец = "Череповец"
    case ярославль = "Ярославль"
}

enum StageTitle: String, Codable {
    case кубокНадежды = "Кубок Надежды"
    case плейОфф = "Плей-офф"
    case регулярныйЧемпионат = "Регулярный чемпионат"
}

enum TypeEnum: String, Codable {
    case playoff = "playoff"
    case regular = "regular"
}

// MARK: - Tables
struct Tables: Codable {
    let championship: [Championship]
//    let divisions: [DivisionElement]
}

// MARK: - DivisionElement
//struct DivisionElement: Codable {
//    let name: DivisionEnum
//    let key: Key
//    let table: [Championship]
//}
