//
//  Player.swift
//  Torpedo
//
//  Created by Artem Soloviev on 08.11.2023.
//

import Foundation




struct PlayerStats: Codable {

    let player: PlayerStat
}


struct PlayerStat: Codable, Identifiable {
    let id, height, weight, age: Int
    let shirtNumber, khlID: Int
    let role, roleKey, country: String
    let image: String
    let flagImageURL: String
    let name: String
    let birthday: Int
    let stick: String
    let team: PurpleTeam
    let stats: [Stat]?
    let teams: [TeamElement]
//    let quotes: [Quote]
    let seasonsCount: SeasonsCount
    let positions: [Position]

    enum CodingKeys: String, CodingKey {
        case id, height, weight, age
        case shirtNumber = "shirt_number"
        case khlID = "khl_id"
        case role
        case roleKey = "role_key"
        case country, image
        case flagImageURL = "flag_image_url"
        case name, birthday, stick, team, stats, teams
//        case quotes
        case seasonsCount = "seasons_count"
        case positions
    }
}

// MARK: - Position
struct Position: Codable {
    let id: String
    let pos: Int
    let title: String
}

// MARK: - Quote
//struct Quote: Codable {
//    let id, startTs, finishTs: Int
//    let description: String
//    let audioTracks: [AudioTrack]
//    let m3U8URL: String
//    let feedURL: String
//    let iframeURL, iframeCode: String
//    let imageURL: String
//    let stageID: Int
//    let free, yandexRights: Bool
//    let yandexID, matchID, balancerType, eventOrQuoteTypeName: String
//    let outerStageID: Int
//    let stageName: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case startTs = "start_ts"
//        case finishTs = "finish_ts"
//        case description
//        case audioTracks = "audio_tracks"
//        case m3U8URL = "m3u8_url"
//        case feedURL = "feed_url"
//        case iframeURL = "iframe_url"
//        case iframeCode = "iframe_code"
//        case imageURL = "image_url"
//        case stageID = "stage_id"
//        case free
//        case yandexRights = "yandex_rights"
//        case yandexID = "yandex_id"
//        case matchID = "match_id"
//        case balancerType = "balancer_type"
//        case eventOrQuoteTypeName = "event_or_quote_type_name"
//        case outerStageID = "outer_stage_id"
//        case stageName = "stage_name"
//    }
//}

struct AudioTrack: Codable {
    let id, title: String
}


struct SeasonsCount: Codable {
    let khl, team: Int
}

struct Stat: Codable, Identifiable {
    let id, title: String?
    let val, max: Double?
    let min: Double?
    let showLabel: Bool?

    enum CodingKeys: String, CodingKey {
        case id, title, val, max, min
        case showLabel = "show_label"
    }
}

struct PurpleTeam: Codable {
    let id, khlID: Int
    let name, location: String
    let image: String
    let division, divisionKey, conference, conferenceKey: String

    enum CodingKeys: String, CodingKey {
        case id
        case khlID = "khl_id"
        case name, location, image, division
        case divisionKey = "division_key"
        case conference
        case conferenceKey = "conference_key"
    }
}


struct TeamElement: Codable {
    let id, khlID: Int
    let name, location, seasons: String
    let image: String

    enum CodingKeys: String, CodingKey {
        case id
        case khlID = "khl_id"
        case name, location, seasons, image
    }
}


