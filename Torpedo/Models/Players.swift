//
//  Players.swift
//  Torpedo
//
//  Created by Artem Soloviev on 27.10.2023.
//



import Foundation

struct Welcome: Codable {
    let team: Team
}

struct Team: Codable {

    let players: [Player]
    let headCoach: HeadCoach?
   
}

struct HeadCoach: Codable {
    let photo: String
    let name: String
}

// MARK: - Player
struct Player: Codable, Identifiable, Hashable {
    let id, khlID: Int
    let image: String
    let flagImageURL: String
    let g, a: Int
    let roleKey: RoleKey
    let name: String
    let shirtNumber: Int
    let country: Country

    enum CodingKeys: String, CodingKey {
        case id
        case khlID = "khl_id"
        case image
        case flagImageURL = "flag_image_url"
        case g, a
        case roleKey = "role_key"
        case name
        case shirtNumber = "shirt_number"
        case country
    }
}

enum Country: String, Codable {
    case беларусь = "Беларусь"
    case россия = "Россия"
    case словакия = "Словакия"
    case сша = "США"
    case япония = "Япония"
    case казахстан = "Казахстан"
}

enum RoleKey: String, Codable {
    case defensemen = "defensemen"
    case forward = "forward"
    case goaltender = "goaltender"
}



//struct Stats: Codable {
////     https://khl.api.webcaster.pro/api/khl_mobile/players_v2.json?application=khl_ios&deviceid=3E52D2EA-B0E2-48F1-8211-8245E5171A5B&locale=en&q%5Bid_in%5D%5B%5D=38867&stage_id=275
//}
//
//import Foundation
//
//// MARK: - Element
//struct Element: Codable {
//    let player: Player
//}
//
//// MARK: - Player
//struct Player: Codable {
//    let id, height, weight, age: Int
//    let shirtNumber, khlID: Int
//    let role: Role
//    let roleKey: RoleKey
//    let country: Country
//    let image: String
//    let flagImageURL: String
//    let name: String
//    let birthday: Int
//    let stick: Stick
//    let team: PurpleTeam?
//    let stats: [Stat]
//    let teams: [TeamElement]
//    let quotes: [Quote]
//    let seasonsCount: SeasonsCount
//    let positions: [Position]
//
//    enum CodingKeys: String, CodingKey {
//        case id, height, weight, age
//        case shirtNumber = "shirt_number"
//        case khlID = "khl_id"
//        case role
//        case roleKey = "role_key"
//        case country, image
//        case flagImageURL = "flag_image_url"
//        case name, birthday, stick, team, stats, teams, quotes
//        case seasonsCount = "seasons_count"
//        case positions
//    }
//}
//
//enum Country: String, Codable {
//    case беларусь = "Беларусь"
//    case россия = "Россия"
//}
//
//// MARK: - Position
//struct Position: Codable {
//    let id: PositionID
//    let pos: Int
//    let title: String
//}
//
//enum PositionID: String, Codable {
//    case a = "a"
//    case g = "g"
//    case gaa = "gaa"
//    case pts = "pts"
//    case so = "so"
//    case svPct = "sv_pct"
//}
//
//// MARK: - Quote
//struct Quote: Codable {
//    let id, startTs, finishTs: Int
//    let description: String
//    let audioTracks: [AudioTrack]
//    let m3U8URL: String
//    let feedURL: String
//    let iframeURL, iframeCode: String
//    let imageURL: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case startTs = "start_ts"
//        case finishTs = "finish_ts"
//        case description
//        case audioTracks = "audio_tracks"
//        case m3U8URL = "m3u8_url"
//        case feedURL = "feed_url"
//        case iframeURL = "iframe_url"
//        case iframeCode = "iframe_code"
//        case imageURL = "image_url"
//    }
//}
//
//// MARK: - AudioTrack
//struct AudioTrack: Codable {
//    let id: AudioTrackID
//    let title: Title
//}
//
//enum AudioTrackID: String, Codable {
//    case aNoise = "a_noise"
//    case main = "main"
//}
//
//enum Title: String, Codable {
//    case интершум = "Интершум"
//    case основная = "Основная"
//}
//
//enum Role: String, Codable {
//    case вратарь = "вратарь"
//    case защитник = "защитник"
//    case нападающий = "нападающий"
//}
//
//enum RoleKey: String, Codable {
//    case defensemen = "defensemen"
//    case forward = "forward"
//    case goaltender = "goaltender"
//}
//
//// MARK: - SeasonsCount
//struct SeasonsCount: Codable {
//    let khl, team: Int
//}
//
//// MARK: - Stat
//struct Stat: Codable {
//    let id, title: String
//    let val: Double?
//    let max: Double
//    let min: Double?
//    let showLabel: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case id, title, val, max, min
//        case showLabel = "show_label"
//    }
//}
//
//enum Stick: String, Codable {
//    case l = "l"
//    case r = "r"
//}
//
//// MARK: - PurpleTeam
//struct PurpleTeam: Codable {
//    let id, khlID: Int
//    let name, location: String
//    let image: String
//    let division, divisionKey: String
//    let conference: Conference
//    let conferenceKey: ConferenceKey
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case khlID = "khl_id"
//        case name, location, image, division
//        case divisionKey = "division_key"
//        case conference
//        case conferenceKey = "conference_key"
//    }
//}
//
//enum Conference: String, Codable {
//    case конференцияВосток = "Конференция «Восток»"
//    case конференцияЗапад = "Конференция «Запад»"
//}
//
//enum ConferenceKey: String, Codable {
//    case east = "east"
//    case west = "west"
//}
//
//// MARK: - TeamElement
//struct TeamElement: Codable {
//    let id, khlID: Int
//    let name, location, seasons: String
//    let image: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case khlID = "khl_id"
//        case name, location, seasons, image
//    }
//}


