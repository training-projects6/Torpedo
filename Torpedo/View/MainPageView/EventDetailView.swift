//
//  EventDetailView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 20.11.2023.
//

import SwiftUI

struct EventDetailView: View {
    @ObservedObject var event: EventViewModel
    var body: some View {
        ZStack(alignment: .top) {
            LinearGradient(colors: [Color("MainColor"), Color("ThirdColor")], startPoint: .bottom, endPoint: .top ) .ignoresSafeArea(.all)
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading){
                    ForEach(event.results) {result in
                        HStack{
                            AsyncImage(url: URL (string: result.teamA.image)) { image in
                                image
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 100)
                                    .padding(.vertical)
                                
                            } placeholder: {
                                ProgressView()
                            }
                            Spacer()
                            VStack(alignment: .center){
                                Text(result.gameStateKey == .finished ? result.eventStartAt.formatted(.dateTime.day().month(.wide).locale(Locale(identifier: "ru_RU"))) : result.eventStartAt.eventDate)
                                if result.gameStateKey == .finished{
                                    Text(result.score)
                                        .font(.largeTitle)
                                }
                                Text(result.location)
                            }
                            Spacer()
                            AsyncImage(url: URL (string: result.teamB.image)) { image in
                                image
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 100)
                                    .padding(.vertical)
                            } placeholder: {
                                ProgressView()
                            }
                        }.padding(.horizontal,10)
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity)
                            .overlay(RoundedRectangle(cornerRadius: 20).strokeBorder(.white, lineWidth: 0.5))
                    }
                }.padding()
            }
        }
    }
}
struct EventDetailView_Previews: PreviewProvider {
    static var previews: some View {
        EventDetailView(event: EventViewModel())
    }
}
