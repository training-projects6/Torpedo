//
//  StageView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 16.11.2023.
//

import SwiftUI

struct StageView: View {
    
    @StateObject private var stage = StageViewModel()
    
    var body: some View {
        VStack{
            HStack(spacing: 30){
                Spacer()
                Text("Игры")
                Text("Побед")
                Text("Пр")
                Text("Очки")
            }
                .fontWeight(.bold)
            
            ScrollView (showsIndicators: false){
                ForEach(Array(zip(stage.stage.indices, stage.stage)), id: \.1) {index, item in
                    HStack{
                        Text("\(index + 1)")
                        AsyncImage(url: URL (string: item.image)) { image in
                            image
                                .resizable()
                                .scaledToFit()
                                .frame(height: 50)
                        } placeholder: {
                            ProgressView()
                        } .padding(.horizontal)
                        Spacer()
                        HStack(alignment: .center) {
                            
                            Text(item.gp)
                                .frame(width:50)
                            DividerView()
                            Text(item.w)
                                .frame(width:50)
                            DividerView()
                            Text(item.l)
                                .frame(width:50)
                            DividerView()
                            Text(item.pts)
                                .frame(width:50)
                        }
                    }.fontWeight(.bold)
                    Divider()
                }
            }
        }
    }
}

struct StageView_Previews: PreviewProvider {
    static var previews: some View {
        StageView()
    }
}
