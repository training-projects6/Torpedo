//
//  MainView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 09.10.2023.
//

import SwiftUI

struct TopScrollView: View {
    @ObservedObject var events: EventViewModel
    @State private var isPresented = false
    var body: some View {
        
        NavigationStack {
            ScrollView(.horizontal, showsIndicators: false){
                HStack(alignment: .center) {
                    ForEach(events.nearGames.prefix(5)) { item in
                        NavigationLink(value: item) {
                            HStack{
                                AsyncImage(url: URL (string: item.teamA.image)) { image in
                                    image
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 80)
                                        .padding(.vertical)
                                    
                                } placeholder: {
                                    ProgressView()
                                }
                                VStack(alignment: .center){
                                    Text(item.eventStartAt.eventDate)
                                    Text(item.location)
                                        .padding(.top)
                                }.padding(.vertical)
                                AsyncImage(url: URL (string: item.teamB.image)) { image in
                                    image
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 80)
                                        .padding(.vertical)
                                } placeholder: {
                                    ProgressView()
                                }
                            }.padding()
                                .foregroundColor(.white)
                                .fontWeight(.bold)
                                .frame(maxWidth: .infinity)
                                .overlay(RoundedRectangle(cornerRadius: 20).strokeBorder(.white, lineWidth: 0.5))
                                .onTapGesture {
                                    isPresented.toggle()
                                }
                        }
                    }
                }.navigationDestination(isPresented: $isPresented) {
                    EventDetailView(event: events)
                }
            }
        }
    }
}


struct MainView: View {
    
    @ObservedObject var evens: EventViewModel
    
    var body: some View {
        
        NavigationStack{
            ZStack(alignment: .top) {
                LinearGradient(colors: [Color("MainColor"), Color("ThirdColor")], startPoint: .bottom, endPoint: .top).ignoresSafeArea(.all)
                ScrollView(showsIndicators: false) {
                    VStack(alignment: .leading){
                        Text("Ближайшие игры")
                            .foregroundColor(.white)
                            .font(.title2)
                            .fontWeight(.bold)
//                            .padding(.horizontal)
                            .padding(.top)
                        Divider()
//                            .padding(.horizontal)
                            .padding(.bottom)
                        TopScrollView(events: evens)
                        
                        Text("Лучшие игроки")
                            .foregroundColor(.white)
                            .font(.title2)
                            .fontWeight(.bold)
//                            .padding(.horizontal)
                            .padding(.top)
                        Divider()
//                            .padding(.horizontal)
                            .padding(.bottom)
                        BestPlayersView()
                        Text("Турнирная таблица")
                            .foregroundColor(.white)
                            .font(.title2)
                            .fontWeight(.bold)
//                            .padding(.horizontal)
                            .padding(.top)
                        Divider()
//                            .padding(.horizontal)
                            .padding(.bottom)
                        StageView()
                            .frame(maxHeight: 500)
                    }
                }.padding()
//                .toolbar{
//                    ToolbarItem(placement:.principal) {
//                        Image("MainLogo")
//                            .resizable()
//                            .scaledToFit()
//                            .frame(height: 65)
//                    }
//                }.navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}

//struct MainView_Previews: PreviewProvider {
//    static var previews: some View {
//        MainView()
//    }
//}
