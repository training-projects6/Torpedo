//
//  MainTicketView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 10.10.2023.
//

import SwiftUI

struct MainTicketView: View {
    
    private var ticket = "https://hctorpedo.ru/tickets/"
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                
                Color("MainColor")
                    .ignoresSafeArea(.all)
                WebViewModel(url: URL(string: ticket)!)
            }
        }
    }
}
struct MainTicketView_Previews: PreviewProvider {
    static var previews: some View {
        MainTicketView()
    }
}
