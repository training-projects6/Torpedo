//
//  MainTeamView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 09.10.2023.
//

import SwiftUI



struct MainTeamView: View {
    
    @EnvironmentObject var players: TeamViewModel
    
    var body: some View {
        NavigationStack {
            ZStack(alignment: .top) {
                LinearGradient(colors: [Color("MainColor"), Color("ThirdColor")], startPoint: .bottom, endPoint: .top).ignoresSafeArea(.all)
                ScrollView(showsIndicators: false) {
                    VStack{
                        ForEach(players.players) {player in
                            NavigationLink(value: player) {
                                HStack{
                                    AsyncImage(url: URL (string: player.image)) { image in
                                        image
                                            .resizable()
                                            .scaledToFit()
                                            .frame(height: 100)
                                            .clipShape(RoundedRectangle(cornerRadius: 20))
                                        
                                    } placeholder: {
                                        ProgressView()
                                    }
                                    Spacer()
                                    VStack(alignment: .trailing){
                                        HStack{
                                            AsyncImage(url: URL (string: player.flagImageURL)) { image in
                                                image
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(height: 50)
                                                    .clipShape(RoundedRectangle(cornerRadius: 20))
                                            } placeholder: {
                                                ProgressView()
                                            }
                                            Text("\(player.shirtNumber)")
                                                .font(.largeTitle)
                                                .foregroundColor(.white)
                                        }
                                        Text(player.name)
                                            .foregroundColor(.white)
                                           
                                    }
                                } .padding()
                                    .frame(maxWidth: .infinity)
                                    .overlay(RoundedRectangle(cornerRadius: 20).strokeBorder(.white, lineWidth: 0.5))
                                    
                                    .onTapGesture {
                                        players.stats(id: player.id)
                                    }
                            }
                        } .fontWeight(.bold)
                    }
                }.padding()
            }
            .navigationDestination(isPresented: $players.isPresented) {
                TeamDetailView()
            }
        }
    }
}

//struct MainTeamView_Previews: PreviewProvider {
//    static var previews: some View {
//        MainTeamView()
//    }
//}
