//
//  BestPlayersView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 09.10.2023.
//

import SwiftUI

struct BestPlayersView: View {
    
    @EnvironmentObject var players: TeamViewModel
    
    var body: some View {
        
        NavigationStack{
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(players.bestPlayers.prefix(5)) {player in
                        NavigationLink(value: player) {
                            VStack(alignment: .leading) {
                                Text(player.name)
                                    .foregroundColor(.white)
                                    .fontWeight(.bold)
                                
                                AsyncImage(url: URL (string: player.image)) { image in
                                    image
                                        .resizable()
                                        .scaledToFill()
                                        .frame(height: 200)
                                        .clipShape(RoundedRectangle(cornerRadius: 20))
                                        .overlay(RoundedRectangle(cornerRadius: 20).strokeBorder(.white, lineWidth: 0.5))
                                } placeholder: {
                                    ProgressView()
                                }
                                
                            }.onTapGesture {
                                players.stats(id: player.id)
                            }
//                            .padding(5)
                        }
                    }
                }
            }.navigationDestination(isPresented: $players.isPresented) {
                TeamDetailView()
            }
        }
    }
}

//struct BestPlayersView_Previews: PreviewProvider {
//    static var previews: some View {
//        BestPlayersView()
//    }
//}
