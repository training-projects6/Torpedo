//
//  TeamDetailView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 28.10.2023.
//

import SwiftUI

struct TeamDetailView: View {
    
    @EnvironmentObject var stats: TeamViewModel
    
    var body: some View {
        
        ZStack(alignment: .top) {
            LinearGradient(colors: [Color("MainColor"), Color("ThirdColor")], startPoint: .bottom, endPoint: .top ) .ignoresSafeArea(.all)
            ScrollView(showsIndicators: false){
                VStack {
                    let stat = stats.playerStat[0]
                    VStack(alignment: .leading) {
                        HStack{
                            Text(stat.name)
                            Spacer()
                            Text("#\(stat.shirtNumber)")
                        }.font(.largeTitle)
                        Text(stat.role)
                            .font(.title)
                    } .padding()
                    
                    AsyncImage(url: URL (string: stat.image)) { image in
                        image
                            .resizable()
                            .scaledToFit()
                            .frame(height: 200)
                            .clipShape(RoundedRectangle(cornerRadius: 20))
                            .overlay(RoundedRectangle(cornerRadius: 20).strokeBorder(.white, lineWidth: 0.5))
                            .shadow(radius: 20)
                    } placeholder: {
                        ProgressView()
                    }
                    HStack{
                        VStack(alignment: .center){
                            Text("возраст")
                            Text("\(stat.age)")
                                .padding(.top, 3)
                        }.padding(.horizontal)
                        DividerView()
                        VStack(alignment: .center){
                            Text("рост")
                            Text("\(stat.height)")
                                .padding(.top, 3)
                        }.padding(.horizontal)
                        DividerView()
                        VStack(alignment: .center){
                            Text("вес")
                            Text("\(stat.weight)")
                                .padding(.top, 3)
                        }.padding(.horizontal)
                        DividerView()
                        VStack(alignment: .center){
                            Text("хват")
                            Text(stat.stick == "l" ? "левый" : "правый")
                                .padding(.top, 3)
                        }.padding(.horizontal)
                    }.padding(.vertical)
                        .overlay(RoundedRectangle(cornerRadius: 20).stroke(.white, lineWidth: 1))
                        .foregroundColor(.white)
                        .font(.headline)
                        .fontWeight(.bold)
                    
                    VStack(alignment: .leading) {
                        Text("Статистика")
                            .font(.headline)
                            .fontWeight(.bold)
                        Divider()
                            .frame(height: 1)
                            .overlay(.white)
                        
                        ForEach(stats.stat){ item in
                            HStack {
                                Text(item.title!)
                                Spacer()
                                Text(item.val!, format: .number.precision(.fractionLength(0)))
                            }.fontWeight(.bold)
                            Divider()
                        }
                    }.padding()
                }
            }
        }
    }
}

struct DividerView: View {
    var body: some View {
        Divider()
            .frame(width: 1, height: 50)
            .overlay(.white)
    }
}

