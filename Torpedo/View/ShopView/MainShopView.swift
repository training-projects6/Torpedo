//
//  MainShopView.swift
//  Torpedo
//
//  Created by Artem Soloviev on 10.10.2023.
//

import SwiftUI

struct MainShopView: View {
    
    private var shop = "https://shop.hctorpedo.ru"
    
    var body: some View {
        ZStack(alignment: .top) {
            
            Color("MainColor")
                .ignoresSafeArea(.all)
            WebViewModel(url: URL(string: shop)!)
        }
    }
}

struct MainShopView_Previews: PreviewProvider {
    static var previews: some View {
        MainShopView()
    }
}
